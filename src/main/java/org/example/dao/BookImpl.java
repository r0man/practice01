package org.example.dao;

import org.example.model.Book;

import java.util.ArrayList;
import java.util.List;

public class BookImpl implements BookDao{

    private final List<Book> books = new ArrayList<Book>(3);

    public BookImpl() {
        books.add(new Book(1, "Book1", 10.0));
        books.add(new Book(2, "Book1", 15.0));
        books.add(new Book(3, "Book1", 20.0));
    }

    public Book getBookById(int id) {
        try{
            return books.get(id);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Cannot find item with id " + id);
            throw new RuntimeException("Cannot find item with id " + id);
        }
    }

    public List<Book> getAll() {
        return books;
    }

    public void add(Book book) {
        books.add(book);
    }

    public Book deleteById(int id) {
        return books.remove(id);
    }
}

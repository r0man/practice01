package org.example.dao;

import org.example.model.Book;

import java.util.List;

public interface BookDao {

    String name = "Ivanov";

    Book getBookById(int id);
    List<Book> getAll();
    void add(Book book);
    Book deleteById(int id);

}

package org.example.exceptions;

public class DiscountException extends RuntimeException{

    public DiscountException(String message) {
        super(message);
    }
}
